# PublicUpdates

Project to manage updates to everyone and anyone - The aim is to reduce redundant issues created across multiple collaboration projects to spread the word about generic updates - e.g. key new features in specific releases.